import { Routes, Route } from "react-router-dom";

import Header from './components/Header'
import Home from './views/Home'
import Profile from "./views/Profile";
import ProtectedRoute from "./components/ProtectedRoute";

const App = () => {
	return (
		<div className="App">
			<Header />
			<Routes>
				<Route
					path="/keepers"
					element={
						<ProtectedRoute>
							<Home />
						</ProtectedRoute>
					}
				/>
				<Route
					path="/keepers/profile"
					element={
						<ProtectedRoute>
							<Profile />
						</ProtectedRoute>
					}
				/>
			</Routes>
		</div>
	);
}

export default App;
