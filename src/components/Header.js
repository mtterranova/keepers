import { Link } from 'react-router-dom'
import { useAuth0 } from '@auth0/auth0-react';

const Header = () => {
    const { user, isAuthenticated } = useAuth0();

    return (
        <div className="header">
            <Link to="/keepers"><h1>Keepers</h1></Link>
            { isAuthenticated ? <Link to="/keepers/profile">Profile</Link> : <div></div> }
            { isAuthenticated && <img className="user-icon" src={user.picture} alt="profile" /> }
        </div>
    )
}

export default Header;